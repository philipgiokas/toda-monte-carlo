/**************************************************************************
** C++ header file for toda_field_lambda_root class                      **                                     **
** Copyright (C) <2019> <Philip Giokas> <philipgiokas@gmail.com>         **
***************************************************************************
** This program is free software: you can redistribute it and/or modify  **
** it under the terms of the GNU General Public License as published by  **
** the Free Software Foundation, either version 3 of the License, or     **
** (at your option) any later version.                                   **
***************************************************************************
** This program is distributed in the hope that it will be useful,       **
** but WITHOUT ANY WARRANTY; without even the implied warranty of        **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
** GNU General Public License for more details.                          **
***************************************************************************
** You should have received a copy of the GNU General Public License     **
** along with this program.  If not, see <https://www.gnu.org/licenses/> **
**************************************************************************/

/*this class is inherited from the toda_field_abstract_class it defines
the necessary values of the alpha and alpha_n vectors for the lambda 
paramterized Toda Field Theories */

#include "toda_field_lambda_root.hpp"
#include <iomanip>

int main(int argc, char *argv[])
{
	int iterations {1 << 18};
	double mass {1.0};
	
	double lambda {atof(argv[1])};
	double beta {atof(argv[2])};
	int length {atoi(argv[3])};
    double decay_length{atof(argv[4])};
   
    std::string str_lambda(argv[1]);
    std::string str_beta(argv[2]);
    std::string str_L(argv[3]);
    std::string str_decay_length(argv[4]);

	std::string file_out_1 {"results/lambda_" + str_lambda + "_B_" + str_beta + "_L_" + str_L + 
	                                      "_wall_corr_decay_length" + str_decay_length + ".dat"};
	                             
	toda_field_lambda_root lam_mass_experiment(lambda, beta, mass, length,iterations, 1 << 5);

	lam_mass_experiment.initialise();
    
    lam_mass_experiment.set_mass(atof(argv[4]), 200000, 0.25);

	for(int N = 0 ; N < iterations ; N++)
	{
		lam_mass_experiment.field_update();
		lam_mass_experiment.sample_wall_correlation_function();
		
		if( N % 10000 == 0) std::cout << N << std::endl;
	}
	
	lam_mass_experiment.write_wall_correlation_function(file_out_1);
	
	return 0;
}

	