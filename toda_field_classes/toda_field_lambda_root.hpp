/**************************************************************************
** C++ header file for toda_field_lambda_root class                      **                                     
** Copyright (C) <2019> <Philip Giokas> <philipgiokas@gmail.com>         **
***************************************************************************
** This program is free software: you can redistribute it and/or modify  **
** it under the terms of the GNU General Public License as published by  **
** the Free Software Foundation, either version 3 of the License, or     **
** (at your option) any later version.                                   **
***************************************************************************
** This program is distributed in the hope that it will be useful,       **
** but WITHOUT ANY WARRANTY; without even the implied warranty of        **
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
** GNU General Public License for more details.                          **
***************************************************************************
** You should have received a copy of the GNU General Public License     **
** along with this program.  If not, see <https://www.gnu.org/licenses/> **
**************************************************************************/

/*this class is inherited from the toda_field_abstract_class it defines
the necessary values of the alpha and alpha_n vectors for the lambda 
paramterized Toda Field Theories */

#ifndef LAMBDAROOTDEF
#define LAMBDAROOTDEF
#include "toda_field_abstract.hpp"

class toda_field_lambda_root : public toda_field_abstract
{
	
	public:
		
		double lambda;
		
		toda_field_lambda_root(double lam, double b, double m, int L, int n_b,
		int n_s_b): toda_field_abstract(b , m , L , n_b , n_s_b)
		{
			lambda=lam;
			alpha_set();
			alpha_n_set();
		}
						
		void alpha_set(void)
		{
			alpha[0][0] = -1.0;
			alpha[0][1] = 1.0;;
			alpha[1][0] = 1.0;
			alpha[1][1] = 0.0;
			alpha[2][0] = - lambda;
			alpha[2][1] = - lambda;

			for(int i = 0 ; i < 3 ; i++)
			{
				beta_alpha[i][0] = beta * alpha[i][0];
				beta_alpha[i][1] = beta * alpha[i][1];
			}
		}
		
		void alpha_n_set(void)
		{
			alpha_n[0] = 1.0;
			alpha_n[1] = 2.0;
			alpha_n[2] = 1.0 / lambda;
		}
};

#endif